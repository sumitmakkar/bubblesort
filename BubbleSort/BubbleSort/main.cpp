#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> listVector;
    
        void swapIndexes(int ind1 , int ind2)
        {
            int temp         = listVector[ind1];
            listVector[ind1] = listVector[ind2];
            listVector[ind2] = temp;
        }
    
    public:
        Engine(vector<int> lV)
        {
            listVector = lV;
        }
    
        void sortList()
        {
            int len = (int)listVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                for(int j = 0 ; j < len - 1 ; j++)
                {
                    if(listVector[j] > listVector[j+1])
                    {
                        swapIndexes(j , j+1);
                    }
                }
            }
        }
    
        void display()
        {
            int len = (int)listVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<listVector[i]<<" ";
            }
            cout<<endl;
        }
};

int main(int argc, const char * argv[])
{
    vector<int> listVector = {12 , 35 , 87 , 26 , 9 , 28 , 7};
    Engine e = Engine(listVector);
    e.sortList();
    e.display();
    return 0;
}
